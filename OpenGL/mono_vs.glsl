// GLSL 3.30
#version 330 core

// iloczyn macierzy Widoku Modelu oraz Projekcji
uniform mat4 modelViewProjectionMatrix;
// macierz Widoku Modelu
uniform mat4 modelViewMatrix;
// kierunek źródła światła
uniform vec3 inLightDir;
// wejsciowy kolor
uniform vec4 inColor;

// współrzędne wierzchołka
in vec3 inVertex;
// współrzędne wektora normalnego w wierzchołku
in vec3 inNormal;

// wektor normalny we współrzędnych obserwatora
out vec3 normal;
// wektork kierunku światła we współrzędnych obserwatora
out vec3 lightDir;
// wektor kierunku "do obserwatora" we współrzędnych obserwatora
out vec3 eye;
// wyjscie koloru
out vec4 color;

void main(void)
{
	// Przekształcenie trójelementowego wektora współrzędnych do czteroelementowego
	vec4 vertex = vec4(inVertex, 1.0);

	// Przekształcenie wektora normalnego do układu obserwatora.
	normal = normalize((modelViewMatrix * vec4(inNormal, 0.0)).xyz);

	//Przepisanie zmiennej wejściowej na wyjściową, aby przekazać ją do kolejnego shadera.
	lightDir = inLightDir;

	// Obliczenie i ustawienie współrzędnych wierzchołka - w układzie obserwatora po
	// wykonaniu operacji rzutowania - macierz Projekcji
	gl_Position = modelViewProjectionMatrix * vertex;

	//przepisz kolor
	color = inColor;
}

