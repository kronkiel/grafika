#include "utils.h"

void initTexture(GLuint* indicesBuffer,
				 GLuint* verticesBuffer, GLuint* verticesLocation, 
				 GLuint* normalsBuffer, GLuint* normalsLocation,
				 GLuint* texturesBuffer, GLuint* texCoordsLocation,
				 GLuint* vertexArray, 
				 GLfloat vertices[], int verticesSize,
				 GLfloat texCoords[], int texCoordsSize,
				 GLuint indices[], int indicesSize)
{
	glGenVertexArrays(1, &*vertexArray);
	glBindVertexArray(*vertexArray);

	// utworzenie obiektu bufora wierzcho�k�w (VBO) i za�adowanie danych
	//wsp�rz�dne
	glGenBuffers(1, &*verticesBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, *verticesBuffer);
	glBufferData(GL_ARRAY_BUFFER, verticesSize, vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(*verticesLocation, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	// normalne:
	glGenBuffers(1, &*normalsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, *normalsBuffer);
	glBufferData(GL_ARRAY_BUFFER, verticesSize, (GLfloat*)vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(*normalsLocation, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	// wsp�rz�dne tekstury:
	glGenBuffers(1, &*texturesBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, *texturesBuffer);
	glBufferData(GL_ARRAY_BUFFER, texCoordsSize, (GLfloat*)texCoords, GL_STATIC_DRAW);
	glVertexAttribPointer(*texCoordsLocation, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	// indeksy:
	glGenBuffers(1, &*indicesBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *indicesBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize, (GLuint*)indices, GL_STATIC_DRAW);

	// w��czenie obiekt�w bufor�w wiercho�k�w
	glEnableVertexAttribArray(*verticesLocation);
	glEnableVertexAttribArray(*normalsLocation);
	glEnableVertexAttribArray(*texCoordsLocation);
}

void initObject(objShape* obj, char* objPath,
				GLuint* verticesBuffer, GLuint* verticesLocation, 
				GLuint* normalsBuffer, GLuint* normalsLocation,
				GLuint* indicesBuffer,
				GLuint* vertexArray)
{
	int objError;
	// wczytanie obiektu z pliku .obj i przygotowanie go
	if ( (objError = (*obj).readFromFile(objPath)) )
		exit(objError);
	// przeskalowanie wczytanego obj, tak aby by� wpisany w jednostkowy sze�cian
	// o �rodku w pocz�tku uk�adu wsp�rz�dnych
	(*obj).scale();

	// sprawdzenie czy zosta�y poprawnie zdefiniowane normalne
	if (!(*obj).nNormals)
		// wygenerowanie u�rednionych normalnych
		(*obj).genSmoothNormals();
		// wygenerowanie normalnych dla �cianek
		//(*obj).genFacesNormals();
	else
		if (!(*obj).normIndGood)
			// gdy indeksy normalnych nie zgadzaj� si� z indeksami wierzho�k�w
			// nale�y przebudowa� obie tablice, aby by�y tak samo indeksowane
			// przbudowanie indeks�w normalnych i je�li trzeba indeks�w wiercho�k�w
			(*obj).rebuildAttribTable('n');	

	// wygenerowanie i w��czenie tablicy wierzcho�k�w .obj
	glGenVertexArrays(1, &(*vertexArray));
	glBindVertexArray(*vertexArray);

	//wsp�rz�dne
	glGenBuffers(1, &(*verticesBuffer));
	glBindBuffer(GL_ARRAY_BUFFER, *verticesBuffer);
	glBufferData(GL_ARRAY_BUFFER, 3*(*obj).nAttribs*sizeof(GLfloat), (*obj).vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(*verticesLocation, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	// normalne:
	glGenBuffers(1, &(*normalsBuffer));
	glBindBuffer(GL_ARRAY_BUFFER, *normalsBuffer);
	glBufferData(GL_ARRAY_BUFFER, 3*(*obj).nAttribs*sizeof(GLfloat), (*obj).normals, GL_STATIC_DRAW);
	glVertexAttribPointer(*normalsLocation, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	// indeksy:
	glGenBuffers(1, &(*indicesBuffer));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *indicesBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3*(*obj).nFaces*sizeof(GLuint), (*obj).faces, GL_STATIC_DRAW);

	// w��czenie obiekt�w bufor�w wiercho�k�w
	glEnableVertexAttribArray(*verticesLocation);
	glEnableVertexAttribArray(*normalsLocation);

	// wy��czenie tablicy wiercho�k�w
	glBindVertexArray(0);
}

void displayTexturedObject(GLMatrixStack* modelViewMatrix, GLGeometryTransform* transformPipeline,
						   M3DVector4f* lightEyeDir, 
						   GLuint* texShader, GLuint *tgaTex,
						   GLuint vertexArray[],
						   float translateZ)
{
	(*modelViewMatrix).PushMatrix();
	// u�ycie obiektu shadera
	glUseProgram(*texShader);

	glUniform3fv(glGetUniformLocation(*texShader, "inLightDir"), 1, *lightEyeDir);
	
	// przesuni�cie uk�adu
	(*modelViewMatrix).Translate(0.0f, 0.0f, translateZ);

	// za�adowanie zmiennej jednorodnej - iloczynu macierzy modelu widoku i projekcji
	glUniformMatrix4fv(glGetUniformLocation(*texShader, "modelViewProjectionMatrix"),
		1, GL_FALSE, (*transformPipeline).GetModelViewProjectionMatrix());

	// za�adowanie zmiennej jednorodnej - transponowanej macierzy modelu widoku
	glUniformMatrix4fv(glGetUniformLocation(*texShader, "modelViewMatrix"),
		1, GL_FALSE, (*transformPipeline).GetModelViewMatrix());

	// za�adowanie zmiennej jednorodnej - identyfikatora tekstury
	glUniform1i(glGetUniformLocation(*texShader, "fileTexture"), 0);
	
	// w��czenie tablicy wierzcho�k�w sze�cianu
	glBindVertexArray(*vertexArray);
	
	// w��czenie aktywnej tekstury
	glBindTexture(GL_TEXTURE_2D, *tgaTex);
	// narysowanie danych zawartych w tablicy wiercho�k�w sze�cianu
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawElements(GL_TRIANGLES, 3*6*2, GL_UNSIGNED_INT, 0);

	// wy��czenie tablic wierzho�k�w
	glBindVertexArray(0);

	// wy��czenie tekstury
	glBindTexture(GL_TEXTURE_2D, *tgaTex);
	
	// zdj�cie zapami�tanej macierzy widoku-mocelu ze stosu
	(*modelViewMatrix).PopMatrix();
}

void displayObject(GLMatrixStack* modelViewMatrix, GLGeometryTransform* transformPipeline,
				   M3DMatrix44f* mCamera, M3DVector4f* lightEyeDir,
				   GLuint *shader, 
				   M3DVector4f* color,
				   GLuint vertexArray[], objShape* obj,
				   GLfloat trX, GLfloat trY, GLfloat trZ,
				   GLfloat rotX, GLfloat rotY, GLfloat rotZ)
{
	// u�ycie obiektu shadera
	glUseProgram(*shader);

	// za�adowanie zmiennej jednorodnej - kierunku �wiatla
	// dalsze przekszta�cenia zmieniaj� macierz Widoku Modelu, ale nie wp�ywaj�
	// na �wiat�o - obracamy obiektami a nie �wiat�em
	glUniform3fv(glGetUniformLocation(*shader, "inLightDir"), 1, *lightEyeDir);

	glUniform4fv(glGetUniformLocation(*shader, "inColor"), 1, *color);

	// Od�o�enie obiektu macierzy na stos
	(*modelViewMatrix).PushMatrix();

	// === przekszta�cenia geometryczne i narysowanie obiekt�w w danym stanie uk�adu ===
	// przesuni�cie uk�adu
	(*modelViewMatrix).Translate(trX, trY, trZ);
	(*modelViewMatrix).Rotate(rotX, 1.0f, 0.0f, 0.0f);
	(*modelViewMatrix).Rotate(rotY, 0.0f, 1.0f, 0.0f);
	(*modelViewMatrix).Rotate(rotZ, 0.0f, 0.0f, 1.0f);

	// za�adowanie zmiennej jednorodnej - iloczynu macierzy modelu widoku i projekcji
	glUniformMatrix4fv(glGetUniformLocation(*shader, "modelViewProjectionMatrix"),
		1, GL_FALSE, (*transformPipeline).GetModelViewProjectionMatrix());

	// za�adowanie zmiennej jednorodnej - transponowanej macierzy modelu widoku
	glUniformMatrix4fv(glGetUniformLocation(*shader, "modelViewMatrix"),
		1, GL_FALSE, (*transformPipeline).GetModelViewMatrix());

	// w��czenie tablicy wierzcho�k�w .obj
	glBindVertexArray(*vertexArray);
	// narysowanie danych zawartych w tablicy wierzcho�k�w .obj
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawElements(GL_TRIANGLES, 3*(*obj).nFaces, GL_UNSIGNED_INT, 0);

	// zdj�cie zapami�tanej macierzy widoku-mocelu ze stosu
	(*modelViewMatrix).PopMatrix();
}

bool loadTGATexture(const char *texFileName)
{
	GLbyte *texPointer;
	int width, height, components;
	GLenum format;
	
	// Read the texture bits
	texPointer = gltReadTGABits(texFileName, &width, &height, &components, &format);
	if(texPointer == NULL) 
		return false;

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, components, width, height, 0, format, GL_UNSIGNED_BYTE, texPointer);

	free(texPointer);
	return true;
}

void loadTexture(GLuint *tgaTex, char *path)
{
	// wygenerowanie i za�adowanie tekstury 
	glGenTextures(1, &(*tgaTex));
	glBindTexture(GL_TEXTURE_2D, *tgaTex);
	// za�adowanie tekstury z pliku .tga
	if (! loadTGATexture(path)) {
		cout << "Nie ma pliku z tekstur�: \"" << path << "\"" << endl;
		exit(1);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
}