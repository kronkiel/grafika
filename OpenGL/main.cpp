#include "utils.h"

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "gltools.lib")

using namespace std;

GLMatrixStack		modelViewMatrix;	// stos macierzy Widoku Modelu
GLMatrixStack		projectionMatrix;	// stos macierzy Projekcji
GLFrustum			viewFrustum;		// bryła obcięcia
GLGeometryTransform	transformPipeline;	// potok renderowania
GLFrame				cameraFrame;		// obiekt kamery

#define PI 3.141592f
#define ANGLE PI/180.0f

struct RGBA {
	GLclampf red;
	GLclampf green;
	GLclampf blue;
	GLclampf alpha;
};

//stałe
//parametry okna
char*		windowTitle			= "Kamil Mielnik";
int			windowWidth			= 640;
int			windowHeight		= 480;
char*		porscheObjPath		= "obj\\porsche.obj";
char*		obstacleObjPath		= "obj\\Humvee.obj";
char*		obstacle2ObjPath	= "obj\\banana.obj";
char*		roadTexPath			= "tga\\ulica.tga";
char*		grassTexPath		= "tga\\trawa.tga";
char*		skyTexPath			= "tga\\niebo.tga";
//parametry globalne
RGBA		backgroundColor		= {0.6f, 0.6f, 0.6f, 1.0f};

//klawiatura
bool specialKeyDown[4];	//left, right, up, down

//samochód
struct CarPosition
{
	float fXPos;
	float fYPos;
	float fZPos;
	float fAngle;
	GLfloat color[4];
};

float lastDisplay = 0;
float difference = 0;
float lastOffset = 0;

static float speed = 500.0f;
static const float driveStep = 0.0035f;
static const float carLeftBorder = -0.6f;
static const float carRightBorder = -carLeftBorder;
static const float carWidth = 0.45f;
static const float carLength = 0.95f;

static const float roadLeftBorder = -1.0f;
static const float roadRightBorder = -roadLeftBorder;
static const float roadLength = 42.0f;
static const float roadNearEnd = 2.0f;
static const float roadFarEnd = -40.0f;
static const float groundYLevel = -0.15f;

static const float grassSize = 100.0f;
static const float skyDepth = roadLength - roadNearEnd;

static const float laneLeft = -0.6f;
static const float laneMiddle = 0.0f;
static const float laneRight = 0.6f;
static const float lanes[3] = {laneLeft, laneMiddle, laneRight};

static float firstDisplay = 0.0f;
static float bestTime = 0.0f;
static const float penalty = 1.0f;

GLfloat carColor[4] = {0.9f, 0.0f, 0.0f, 1.0f};
GLfloat obstacleColor[4] = {0.0f, 0.0f, 0.7f, 1.0f};
static CarPosition carData = {0.0f, 0.0f, 0.0f, 0.0f, *carColor};
static const int obstaclesCount = 10;
static CarPosition obstacles[obstaclesCount] = {
		{laneLeft,		carData.fYPos,	-37.0f,	180 * ANGLE, *obstacleColor},
		{laneMiddle,	carData.fYPos,	-34.0f,	180 * ANGLE, *obstacleColor},
		{laneRight,		carData.fYPos,	-31.0f,	180 * ANGLE, *obstacleColor},
		{laneMiddle,	carData.fYPos,	-28.0f,	180 * ANGLE, *obstacleColor},
		{laneLeft,		carData.fYPos,	-25.0f,	180 * ANGLE, *obstacleColor},
		{laneRight,		carData.fYPos,	-22.0f,	180 * ANGLE, *obstacleColor},
		{laneMiddle,	carData.fYPos,	-19.0f,	180 * ANGLE, *obstacleColor},
		{laneRight,		carData.fYPos,	-16.0f,	180 * ANGLE, *obstacleColor},
		{laneMiddle,	carData.fYPos,	-13.0f,	180 * ANGLE, *obstacleColor},
		{laneLeft,		carData.fYPos,	-10.0f,	180 * ANGLE, *obstacleColor},
	};

static bool run = false;

void checkForCollision(CarPosition position)
{
	if(abs(carData.fXPos -  position.fXPos) < carWidth
		&& abs(carData.fZPos - position.fZPos) < carLength)
	{
		run = false;
		firstDisplay = glutGet(GLUT_ELAPSED_TIME) / speed;
	}
}

void generateObstacles()
{
	float startY = -10;
	for(int i = 0; i < obstaclesCount; ++i)
	{
		obstacles[i].fXPos = lanes[rand() % 3];
		obstacles[i].fZPos = startY - 4 * i;
		for(int j = 0; j < 3; ++j)
		{
			obstacles[i].color[j] = (1.0f * rand()) / RAND_MAX;
		}
	}
}

void driveLeft(CarPosition *carData, float step = driveStep)
{
	if((*carData).fXPos > carLeftBorder)
		(*carData).fXPos -= step * difference * speed;
}

void driveRight(CarPosition *carData, float step = driveStep)
{
	if((*carData).fXPos < carRightBorder)
		(*carData).fXPos += step * difference * speed;
}

// kierunek źródła światła
GLfloat lightDir[4] = {0.0f, 45.0f, 90.0f, 1.0f};

// identyfikator obiektu programu
GLuint monoShader, texShader;

// identyfikatory obiektów tablic wierchołków dla .obj i sześcianu
GLuint porscheVertexArray;
GLuint obstacleVertexArray;
GLuint obstacle2VertexArray;
GLuint roadVertexArray;
GLuint grassVertexArray;
GLuint skyVertexArray;

// obiekt reprezentujący bryłę wczytaną jako .obj
objShape porscheObj;
objShape obstacleObj;
objShape obstacle2Obj;

// identyfikator tekstury
GLuint roadTgaTex;
GLuint grassTgaTex;
GLuint skyTgaTex;

//=============================================================================
// inicjalizacja stałych elementów maszyny stanu OpenGL
//=============================================================================
int init()
{
	//-----------------------------------------
	//trawa:
	// współrzędne wierzchołków
	GLfloat grassVertices[4*3] = {
		-grassSize,		groundYLevel-0.0001f,		grassSize,
		grassSize,		groundYLevel-0.0001f,		grassSize,
		-grassSize,		groundYLevel-0.0001f,		-grassSize,
		grassSize,		groundYLevel-0.0001f,		-grassSize,
	};

	// normalne - nie ma potrzeby ich definiowania.
	// Współrzędne wierzchołków są zarazem wektorami nomralnymi (po ich unormowaniu).

	// współrzędne tekstur dla ścian
	GLfloat grassTexCoords[4*2] = {
		0.0f, 0.0f,
		grassSize, 0.0f,
		0.0f, grassSize,
		grassSize, grassSize
	};

	// indeksy
	GLuint grassIndices[3*2] = {
		0, 1, 2,
		2, 1, 3,
	};

	//-----------------------------------------
	//droga:
	// współrzędne wierzchołków
	GLfloat roadVertices[4*3] = {
		roadLeftBorder,		groundYLevel,		roadNearEnd,
		roadRightBorder,	groundYLevel,		roadNearEnd,
		roadLeftBorder,		groundYLevel,		roadFarEnd,
		roadRightBorder,	groundYLevel,		roadFarEnd
	};

	// normalne - nie ma potrzeby ich definiowania.
	// Współrzędne wierzchołków są zarazem wektorami nomralnymi (po ich unormowaniu).

	// współrzędne tekstur dla ścian
	GLfloat roadTexCoords[4*2] = {
		0.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, roadLength,
		1.0f, roadLength
	};

	// indeksy
	GLuint roadIndices[3*2] = {
		0, 1, 2,
		2, 1, 3,
	};

	//niebo:
	// współrzędne wierzchołków
	GLfloat skyVertices[4*3] = {
		-grassSize,		-grassSize,		-skyDepth,
		grassSize,		-grassSize,		-skyDepth,
		-grassSize,		grassSize,		-skyDepth,
		grassSize,		grassSize,		-skyDepth,
	};

	// normalne - nie ma potrzeby ich definiowania.
	// Współrzędne wierzchołków są zarazem wektorami nomralnymi (po ich unormowaniu).

	// współrzędne tekstur dla ścian
	GLfloat skyTexCoords[4*2] = {
		0.0f, 0.0f,
		10.0f, 0.0f,
		0.0f, 10.0f,
		10.0f, 10.0f
	};

	// indeksy
	GLuint skyIndices[3*2] = {
		0, 1, 2,
		2, 1, 3,
	};

	int objError;
	// identyfikatory obiektów buforów ze współrzędnymi, normalnymi i indeksami
	GLuint verticesBuffer;
	GLuint normalsBuffer;
	GLuint indicesBuffer;
	GLuint texturesBuffer;
	// indeksy atrybutów wierzchołków (współrzędne i normalne) z obiektu shadera
	GLuint verticesLocation;
	GLuint normalsLocation;
	GLuint texCoordsLocation;
	GLuint colorLocation;

	// ustawienie koloru tła
	glClearColor(backgroundColor.red, backgroundColor.green, backgroundColor.blue, backgroundColor.alpha);
	
	AttachVertexShader(monoShader, "mono_vs.glsl");
	AttachFragmentShader(monoShader, "mono_fs.glsl");
	// wykonanie powiązania pomiędzy zmienną a indeksem ogólnych atrybutów wierzchołka
	// operacja ta nie jest konieczna i będzie wykonana automatycznie w czasie
	// konsolidacji shadera - przypisany zostanie "pierwszy wolny" indeks
	LinkProgram(monoShader);
	
	// lokalizacja (indeksy) zmiennych w shaderze
	verticesLocation = glGetAttribLocation(monoShader, "inVertex");
	normalsLocation = glGetAttribLocation(monoShader, "inNormal");

	initObject(
		&porscheObj, porscheObjPath, 
		&verticesBuffer, &verticesLocation,
		&normalsBuffer, &normalsLocation,
		&indicesBuffer,
		&porscheVertexArray);

	initObject(
		&obstacleObj, obstacleObjPath,
		&verticesBuffer, &verticesLocation,
		&normalsBuffer, &normalsLocation,
		&indicesBuffer,
		&obstacleVertexArray);

	initObject(
		&obstacle2Obj, obstacle2ObjPath,
		&verticesBuffer, &verticesLocation,
		&normalsBuffer, &normalsLocation,
		&indicesBuffer,
		&obstacle2VertexArray);

	// wczytanie shaderów i przygotowanie obsługi programu
	AttachVertexShader(texShader, "texture_vs.glsl");
	AttachFragmentShader(texShader, "texture_fs.glsl");
	// wykonanie powiązania pomiędzy zmienną a indeksem ogólnych atrybutów wierzchołka
	// operacja ta nie jest konieczna i będzie wykonana automatycznie w czasie
	// konsolidacji shadera - przypisany zostanie "pierwszy wolny" indeks
	LinkProgram(texShader);

	// lokalizacja (indeksy) zmiennych w shaderze
	verticesLocation = glGetAttribLocation(texShader, "inVertex");
	normalsLocation = glGetAttribLocation(texShader, "inNormal");
	texCoordsLocation = glGetAttribLocation(texShader, "inTexCoord");

	//trawa
	initTexture(
		&indicesBuffer,
		&verticesBuffer, &verticesLocation,
		&normalsBuffer, &normalsLocation, 
		&texturesBuffer, &texCoordsLocation,
		&grassVertexArray, 
		grassVertices, sizeof(grassVertices),
		grassTexCoords, sizeof(grassTexCoords),
		grassIndices, sizeof(grassIndices));

	//droga
	initTexture(
		&indicesBuffer,
		&verticesBuffer, &verticesLocation,
		&normalsBuffer, &normalsLocation, 
		&texturesBuffer, &texCoordsLocation,
		&roadVertexArray, 
		roadVertices, sizeof(roadVertices),
		roadTexCoords, sizeof(roadTexCoords),
		roadIndices, sizeof(roadIndices));

	//niebo
	initTexture(
		&indicesBuffer,
		&verticesBuffer, &verticesLocation,
		&normalsBuffer, &normalsLocation, 
		&texturesBuffer, &texCoordsLocation,
		&skyVertexArray, 
		skyVertices, sizeof(skyVertices),
		skyTexCoords, sizeof(skyTexCoords),
		skyIndices, sizeof(skyIndices));

	loadTexture(&roadTgaTex, roadTexPath);
	loadTexture(&grassTgaTex, grassTexPath);
	loadTexture(&skyTgaTex, skyTexPath);

	// włączenie wykorzystania bufora głębokości
	glEnable(GL_DEPTH_TEST);
	// włączenie pominięcia renderowania tylnych stron wielokątów
	glEnable(GL_CULL_FACE);

	// ustawienie początkowego położenia kamery
	cameraFrame.SetOrigin(0.0f, 2.0f, 3.0f);
	cameraFrame.RotateLocalX(0.35f);
	// zdefiniowanie potoku renderowania składającego się z dwóch stosów macierzy
	transformPipeline.SetMatrixStacks(modelViewMatrix, projectionMatrix);
	return 0;
}

//=============================================================================
// zmiana rozmiaru okna
//=============================================================================
void reshape(int width, int height)
{
	// ustawienie obszaru renderingu - całe okno
	glViewport(0, 0, width, height);
	// utworzenie bryły obcięcia określającej perspektywę
	viewFrustum.SetPerspective(55.0f, float(width)/float(height), 1.0f, 100.0f);
	// załadowanie macierzy opisującej bryłę obcięcia do macierzy Projekcji
	projectionMatrix.LoadMatrix(viewFrustum.GetProjectionMatrix());
}


//=============================================================================
// obsługa klawiatury
//=============================================================================
void handleSpecialKeyboard()
{
	for(int key = 0; key < 4; key++)
	{
		if(specialKeyDown[key] == true)
		{
			switch(key)
			{
				case 0:		//left
					driveLeft(&carData);
					break;
				case 1:		//right
					driveRight(&carData);
					break;
				case 2:		//up
					break;
				case 3:		//down
					break;
			}
		}
	}
}

void specialKeyDownFunc (int key, int x, int y)
{
	// obsługa klawiszy funkcyjnych - analogicznie jak podstawowych
	switch (key) 
	{
		case GLUT_KEY_LEFT:
			specialKeyDown[0] = true;
			break;
		case GLUT_KEY_RIGHT: 
			specialKeyDown[1] = true;
			break;
		case GLUT_KEY_UP: 
			specialKeyDown[2] = true;
			break;
		case GLUT_KEY_DOWN: 
			specialKeyDown[3] = true;
			break;
	}
}

void specialKeyUpFunc (int key, int x, int y)
{
	// obsługa klawiszy funkcyjnych - analogicznie jak podstawowych
	switch (key) 
	{
		case GLUT_KEY_LEFT:
			specialKeyDown[0] = false;
			break;
		case GLUT_KEY_RIGHT: 
			specialKeyDown[1] = false;
			break;
		case GLUT_KEY_UP: 
			specialKeyDown[2] = false;
			break;
		case GLUT_KEY_DOWN: 
			specialKeyDown[3] = false;
			break;
	}
}

void keyDownFunc(unsigned char key, int x, int y)
{
	objShape tmp = obstacleObj;
	GLuint tmp2 = obstacleVertexArray;
	// obsługa standardowych klawiszy

	if (key >= '0' && key <= '9')
	{
		lastOffset += (lastDisplay - firstDisplay) * (roadNearEnd - roadFarEnd) / 10;
		lastDisplay *= speed;
	}

	switch (key) {
		case 'a':
			obstacleObj = obstacle2Obj;
			obstacle2Obj = tmp;
			obstacleVertexArray = obstacle2VertexArray;
			obstacle2VertexArray = tmp2;
			break;
		case '0': 
			speed = MAXINT;
			break;
		case '1':
			speed = 1500;
			break;
		case '2':
			speed = 1000;
			break;
		case '3':
			speed = 750;
			break;
		case '4':
			speed = 625;
			break;
		case '5':
			speed = 500;
			break;
		case '6':
			speed = 400;
			break;
		case '7':
			speed = 350;
			break;
		case '8':
			speed = 300;
			break;
		case '9':
			speed = 250;
			break;
		case 27: exit(0);
	}

	if (key >= '0' && key <= '9')
	{
		printf("Predkosc: %d\n", key - '0');
		lastDisplay /= speed;
		firstDisplay = lastDisplay;
	}
}


//=============================================================================
// wyświetlenie sceny
//=============================================================================
void display(void)
{
	glutPostRedisplay();

	float fElapsedTime = glutGet(GLUT_ELAPSED_TIME) / speed;
	
	float translateZ = (fElapsedTime - floor(fElapsedTime)) * (roadNearEnd - roadFarEnd) / 10;
	float translateObstaclesZ = lastOffset + (fElapsedTime - firstDisplay) * (roadNearEnd - roadFarEnd) / 10;

	if(!run)
	{
		if(fElapsedTime - firstDisplay > 500 / speed)
		{
			generateObstacles();
			firstDisplay = fElapsedTime;
			lastOffset = 0.0f;
			carData.fXPos = 0.0f;
			run = true;
		}
		return;
	}
	
	handleSpecialKeyboard();

	// czyszczenie bufora koloru
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	M3DMatrix44f mCamera;
	// pobranie macierzy 4x4 (z obiektu kamery) zawierającej przekształcenia kamery
	cameraFrame.GetCameraMatrix(mCamera);
	// załadowanie przekształceń kamery do stosu macierzy Widoku Modelu
	// (nie ma zatem potrzeby uprzedniego ładowania macierzy jednostkowej)
	modelViewMatrix.LoadMatrix(mCamera);

	M3DVector4f lightEyeDir;
	// przetransformowanie kierunku światła do współrzędnych obserwatora
	m3dTransformVector4(lightEyeDir, lightDir, mCamera);

	//rysuj autko:
	displayObject(
		&modelViewMatrix, &transformPipeline,
		&mCamera, &lightEyeDir,
		&monoShader,
		&carData.color,
		&porscheVertexArray, &porscheObj,
		carData.fXPos, carData.fYPos, carData.fZPos,
		0.0f, carData.fAngle, 0.0f);

	//rysuj przeszkody:
	for(int i = 0; i < obstaclesCount; ++i)
	{
		CarPosition currentPosition = { obstacles[i].fXPos, obstacles[i].fYPos, obstacles[i].fZPos + translateObstaclesZ, obstacles[i].fAngle};
		checkForCollision(currentPosition);
		displayObject(
			&modelViewMatrix, &transformPipeline,
			&mCamera, &lightEyeDir,
			&monoShader,
			&obstacles[i].color,
			&obstacleVertexArray, &obstacleObj,
			obstacles[i].fXPos, obstacles[i].fYPos, obstacles[i].fZPos + translateObstaclesZ,
			0.0f, obstacles[i].fAngle, 0.0f);
		if(obstacles[i].fZPos + translateObstaclesZ > roadNearEnd)
		{
			obstacles[i].fZPos -= roadLength;
			obstacles[i].fXPos = lanes[rand() % 3];
			for(int j = 0; j < 3; ++j)
			{
				obstacles[i].color[j] = (1.0f * rand()) / RAND_MAX;
			}
		}
	}

	//rysuj trawe:
	displayTexturedObject(
		&modelViewMatrix, &transformPipeline, 
		&lightEyeDir, 
		&texShader, &grassTgaTex,
		&grassVertexArray, translateZ);

	//rysuj droge:
	displayTexturedObject(
		&modelViewMatrix, &transformPipeline, 
		&lightEyeDir, 
		&texShader, &roadTgaTex,
		&roadVertexArray, translateZ);

	//rysuj niebo:
	displayTexturedObject(
		&modelViewMatrix, &transformPipeline, 
		&lightEyeDir, 
		&texShader, &skyTgaTex,
		&skyVertexArray);
	

	// wyłączenie shadera
	glUseProgram(0);

	// wyrenderowanie sceny
	glFlush();
	// wymuszenie odrysowania okna
	// (wywołanie zarejestrowanej funcji do obsługi tego zdarzenia)

	difference = fElapsedTime - lastDisplay;
	lastDisplay = fElapsedTime;
}

//=============================================================================
// główna funkcja programu
//=============================================================================
int main(int argc, char** argv)
{
	gltSetWorkingDirectory(argv[0]);
	// ustalenie odpowiedniego kontekstu renderowania
	glutInitContextVersion(3, 3);
	glutInitContextFlags(GLUT_DEBUG);
	// określenie wykorzystywanego profilu - profil CORE pełna zgodność z v3.2
	glutInitContextProfile(GLUT_CORE_PROFILE);
	// inicjalizacja biblioteki GLUT
	glutInit(&argc, argv);
	// określenie trybu pracy biblioteki - kolor w formacie RGB
	glutInitDisplayMode(GLUT_RGB);
	// rozmiar tworzonego okna (w pikselach)
	glutInitWindowSize(windowWidth, windowHeight);
	// położenie okna na ekranie - na środku
	glutInitWindowPosition((glutGet(GLUT_SCREEN_WIDTH) - windowWidth) / 2, (glutGet(GLUT_SCREEN_HEIGHT) - windowHeight) / 2);
	// stworzenie okna programu
	glutCreateWindow(windowTitle);

	// inicjalizacja biblioteki GLEW
	glewExperimental = GL_TRUE;
	GLenum glewErr = glewInit();
	// sprawdzenie poprawności inicjalizacji GLEWa
	if (GLEW_OK != glewErr) {
		// nieudana inicjalizacja biblioteki
		cout << "Blad glewInit: " << glewGetErrorString(glewErr) << endl;
		return 2;
	}

	// wykonanie czynności przygotowawczych programu
	if (init())
		return 3;

	// ======================   funkcje callback ==================================
	// funkcja obsługująca zdarzenie konieczności odrysowania okna
	glutDisplayFunc(display);
	// funkcja obsługująca zdarzenie związane ze zmianą rozmiaru okna
	glutReshapeFunc(reshape);
	// funkcja obsługująca naciśnięcie standardowego klawisza z klawiatury
	glutKeyboardFunc(keyDownFunc);
	// funkcja obsługująca naciśnięcie klawisza specjalnego z klawiatury
	glutSpecialFunc(specialKeyDownFunc);
	glutSpecialUpFunc(specialKeyUpFunc);
 	//=============================================================================
	// główna pętla programu
	glutMainLoop();

	return 0;
}


