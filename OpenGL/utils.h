// do��czenie plik�w nag��wkowych z biblioteki J.Ganczarskiego
#include "shaders.h"
#include "vecmatquat.h"
#include "obj.h"
#include "targa.h"

// za�adowanie pliku nag��wkowego biblioteki GLEW - znajduj� si� w nim wszystkie
// rozszerzenia biblioteki OpenGL r�wnie� dla wersji wy�szych ni� 3.0
// Nie ma wi�c potrzeby wykorzysytwania pliku nag��wkowego gl3.h
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/gl.h>

#include <GLTools.h>
#include <GLBatch.h>
#include <GLMatrixStack.h>
#include <GLGeometryTransform.h>
#include <GLFrustum.h>

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

void initTexture(GLuint* indicesBuffer,
				 GLuint* verticesBuffer, GLuint* verticesLocation, 
				 GLuint* normalsBuffer, GLuint* normalsLocation,
				 GLuint* texturesBuffer, GLuint* texCoordsLocation,
				 GLuint* vertexArray, 
				 GLfloat vertices[], int verticesSize,
				 GLfloat texCoords[], int texCoordsSize,
				 GLuint indices[], int indicesSize);

void initObject(objShape* obj, char* objPath,
				GLuint* verticesBuffer, GLuint* verticesLocation, 
				GLuint* normalsBuffer, GLuint* normalsLocation,
				GLuint* indicesBuffer,
				GLuint* vertexArray);

void displayTexturedObject(GLMatrixStack* modelViewMatrix, GLGeometryTransform* transformPipeline,
						   M3DVector4f* lightEyeDir, 
						   GLuint* texShader, GLuint *tgaTex,
						   GLuint vertexArray[],
						   float translateZ = 0.0f);

void displayObject(GLMatrixStack* modelViewMatrix, GLGeometryTransform* transformPipeline,
				   M3DMatrix44f* mCamera, M3DVector4f* lightEyeDir,
				   GLuint *shader, 
				   M3DVector4f* color,
				   GLuint vertexArray[], objShape* obj,
				   GLfloat trX, GLfloat trY, GLfloat trZ,
				   GLfloat rotX, GLfloat rotY, GLfloat rotZ);

bool loadTGATexture(const char *texFileName);

void loadTexture(GLuint *tgaTex, char *path);